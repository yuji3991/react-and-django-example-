# React & Django FullStack App

### 概要
簡易的なフルスタックのアプリを作成する目標のため、DjangoとReactを使用してアプリを作成

### 使用できる機能
・氏名、メールアドレス、メッセージを登録、表示、削除する

### フレームワーク
>Django

>React

### セットアップ
>$ pipenv shell

>$ pipenv install django djangorestframework django-rest-knox

### バージョン
>Python 3.5

### 参考URL
>https://www.django-rest-framework.org/

>https://reactjs.org/
